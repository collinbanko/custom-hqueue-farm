# Custom HQueue Farm #

Submitting a job to HQueue requires a fair amount of premeditated thought and much trial and error. In an effort to alliveiate the artist of this harship, I have created a custom frontend for users to submit their job with ease. In addition, the process of setting up a number of nodes, or clients, has been made easier through a number of scripts.

### About HQueue ###

HQueue is a distributed simulation rendering backend for SideFX's procedural Visual Effects program Houdini. An HQueue setup consists of a primary server, a network storage server, and a number of nodes, or clients. 

### Prerequisites ###

* Server
* Network Storage Server
* Nodes (clients)
* CentOS/RHEL (version >= 6.5)
* Houdini (version >= 13.0.237) [[download]][houdini-13.0.237]

## Setup Network Storage Server ##

On a clean installation of CentOS/RHEL run the following. This will setup a Samba network file system and any required RPMs or services. 

    ./storage_setup.sh [user_password]


[houdini-13.0.237]: https://bitbucket.org/collinbanko/custom-hqueue-farm/downloads/houdini-13.0.237.tar.gz
