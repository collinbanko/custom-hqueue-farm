#!/bin/sh

clear;

###############################
# Ask for Password Argument #
###############################

echo "Password for HQuser:"; read -s PASSWORD

##########################
# Install Samba Packages #
##########################

yum install -y samba samba-client samba-common

############################
# Start Samaba and NetBIOS #
############################

service smb start
service nmb start

#####################################
# Add Samaba and NetBIOS to Startup #
#####################################

chkconfig --add smb
chkconfig --level 345 smb on
chkconfig --add nmb
chkconfig --level 345 nmb on

#############################
# Create User with Password #
#############################

useradd -c "HQueue User" hquser
echo -e "$PASSWORD" | passwd hquser --stdin
echo -e "$PASSWORD\n$PASSWORD" | smbpasswd -s -a hquser

###############################
# Samaba File System Settings #
###############################

mkdir -p /hq
SAMBA_SETTINGS=samba_file_system.txt
chmod a+w /hq
chcon -t samba_share_t /hq
if ! grep "[hq]" /etc/samba/smb.conf -q; then
    cat "$SAMBA_SETTINGS" >> /etc/samba/smb.conf
fi

####################
# Disable Firewall #
####################

service iptables save
service iptables stop
chkconfig iptables off

##############################
# Restart Samaba and NetBIOS #
##############################

service smb restart
service nmb restart