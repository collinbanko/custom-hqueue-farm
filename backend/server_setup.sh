#!/bin/sh

clear;

###############################
# Ask for Password Argument #
###############################

echo "Password for HQuser:"; read -s PASSWORD

###################
# Install Houdini #
###################

mkdir -p /opt/hfs13.0.288/python

echo "Extracting all files."
tar -zxvf houdini-13.0.288-linux_x86_64_gcc4.4.tar.gz  >/dev/null 2>&1

pushd houdini-13.0.288-linux_x86_64_gcc4.4
echo "Extracting Houdini installation."
tar -zxvf houdini.tar.gz -C /opt/hfs13.0.288 >/dev/null 2>&1
echo "Extracting Python 2.6 library."
tar -zxvf python2.6.tar.gz -C /opt/hfs13.0.288/python >/dev/null 2>&1
echo "Extracting Python 2.7 library."
tar -zxvf python2.7.tar.gz -C /opt/hfs13.0.288/python >/dev/null 2>&1
echo "Extracting Python libs."
tar -zxvf pythonlibdeps.tar.gz -C /opt/hfs13.0.288/python >/dev/null 2>&1
popd

rm -rf houdini-13.0.288-linux_x86_64_gcc4.4

##################
# Install HQueue #
##################

# still need to do this.

###########################
# Setup Houdini Licensing #
###########################

cd /opt/hfs-13.0.237
source houdini_setup
hserver -S houdiniserver.scad.edu

#######################
# Add startup scripts #
#######################

if [ ! -f /etc/rc.d/init.d/hqserverd ]; then
    cp /opt/hqueue/scripts/hqserverd /etc/rc.d/init.d/
fi
if [ ! -L /etc/rc0.d/K96hqserverd ]; then
    ln -s /etc/rc.d/init.d/hqserverd /etc/rc0.d/K96hqserverd
fi
if [ ! -L /etc/rc1.d/K96hqserverd ]; then
    ln -s /etc/rc.d/init.d/hqserverd /etc/rc1.d/K96hqserverd
fi
if [ ! -L /etc/rc2.d/S96hqserverd ]; then
    ln -s /etc/rc.d/init.d/hqserverd /etc/rc2.d/S96hqserverd
fi
if [ ! -L /etc/rc3.d/S96hqserverd ]; then
    ln -s /etc/rc.d/init.d/hqserverd /etc/rc3.d/S96hqserverd
fi
if [ ! -L /etc/rc4.d/S96hqserverd ]; then
    ln -s /etc/rc.d/init.d/hqserverd /etc/rc4.d/S96hqserverd
fi
if [ ! -L /etc/rc5.d/S96hqserverd ]; then
    ln -s /etc/rc.d/init.d/hqserverd /etc/rc5.d/S96hqserverd
fi
if [ ! -L /etc/rc6.d/K96hqserverd ]; then
    ln -s /etc/rc.d/init.d/hqserverd /etc/rc6.d/K96hqserverd
fi

#########################
# Run HQueue Daemon now #
#########################

/etc/rc.d/init.d/hqserverd restart

#############################
# Create User with Password #
#############################

useradd -c "HQueue User" hquser
echo -e "$PASSWORD" | passwd hquser --stdin

########################################
# Create directory for network storage #
########################################

mkdir -p /hq
chmod a+w /hq
chcon -t samba_share_t /hq

############################
# Mount shared samba drive #
############################

yum install -y samba samba-client samba-common
if mountpoint -q /hq; then
    mount.cifs //10.7.125.13/hq /hq -o username=hquser sec=krb5
fi